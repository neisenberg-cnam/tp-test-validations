﻿using System;
using System.Collections.Generic;
using Bank;
using Bank.Entities;
using Bank.Enums;
using Bank.Exceptions;
using Xunit;

namespace BankTests
{
    public class Tests
    {
        private static Loanee Loanee = new Loanee(
            new Dictionary<InsuranceCriteria, bool>()
            {
                {InsuranceCriteria.Fit, false},
                {InsuranceCriteria.Smoker, false},
                {InsuranceCriteria.HeartDisease, false},
                {InsuranceCriteria.SoftwareEngineer, false},
                {InsuranceCriteria.FighterPilot, false},
            }, 28);

        private static Loanee LoaneeQ1 = new Loanee(
            new Dictionary<InsuranceCriteria, bool>()
            {
                {InsuranceCriteria.Fit, false},
                {InsuranceCriteria.Smoker, true},
                {InsuranceCriteria.HeartDisease, true},
                {InsuranceCriteria.SoftwareEngineer, true},
                {InsuranceCriteria.FighterPilot, false},
            }, 28);
        
        private static Loanee LoaneeQ2 = new Loanee(
            new Dictionary<InsuranceCriteria, bool>()
            {
                {InsuranceCriteria.Fit, true},
                {InsuranceCriteria.Smoker, false},
                {InsuranceCriteria.HeartDisease, false},
                {InsuranceCriteria.SoftwareEngineer, false},
                {InsuranceCriteria.FighterPilot, true},
            }, 28);
        
        private static double Capital = 120000;
        private static double CapitalQ1 = 175000;
        private static double CapitalQ2 = 200000;
        private static LoanCalculator LoanCalculator = new LoanCalculator(Capital, Loanee, 10, RateEnum.VeryGood);
        private static LoanCalculator LoanCalculatorQ1 = new LoanCalculator(CapitalQ1, LoaneeQ1, 25, RateEnum.Good);
        private static LoanCalculator LoanCalculatorQ2 = new LoanCalculator(CapitalQ2, LoaneeQ2, 15, RateEnum.VeryGood);
            
        [Fact]
        public void LoanAmountError()
        {
            Assert.Throws<LoanAmountException>(() => new LoanCalculator(0, Loanee, 10, RateEnum.VeryGood));
        }
        
        [Fact]
        public void LoanLengthError()
        {
            Assert.Throws<LoanLengthException>(() => new LoanCalculator(Capital, Loanee, 1, RateEnum.VeryGood));
            Assert.Throws<LoanLengthException>(() => new LoanCalculator(Capital, Loanee, 50, RateEnum.VeryGood));
        }
        [Fact]
        public void LoanTrivialValues()
        {
            Assert.Equal( 12696, LoanCalculator.AmountRepaidAfterYears(1));
        }

        [Fact]
        public void LoanQuestionOneValues()
        {
            Assert.Equal(783, LoanCalculatorQ1.ReadableMonthlyRate());
            Assert.Equal(102, LoanCalculatorQ1.ReadableMonthlyInsuranceCost());
            Assert.Equal(59966, LoanCalculatorQ1.ReadableRealInterestCost());
            Assert.Equal(30625, LoanCalculatorQ1.ReadableRealInsuranceCost());
            Assert.Equal(93960, LoanCalculatorQ1.AmountRepaidAfterYears(10));
        }
        
        [Fact]
        public void LoanQuestionTwoValues()
        {
            Assert.Equal(1240, LoanCalculatorQ2.ReadableMonthlyRate());
            Assert.Equal(67, LoanCalculatorQ2.ReadableMonthlyInsuranceCost());
            Assert.Equal(23211, LoanCalculatorQ2.ReadableRealInterestCost());
            Assert.Equal(12000, LoanCalculatorQ2.ReadableRealInsuranceCost());
            Assert.Equal(148800, LoanCalculatorQ2.AmountRepaidAfterYears(10));
        }
    }
}