namespace WebApplication1;

public class AskForLoanData
{
    public int Capital { get; set; }
    public int Length { get; set; }
    public string RateType { get; set; }
    public bool Fit { get; set; }
    public bool Smoker { get; set; }
    public bool HeartDisease { get; set; }
    public bool SoftwareEngineer { get; set; }
    public bool FighterPilot { get; set; }
    public int NbYears { get; set; }
}