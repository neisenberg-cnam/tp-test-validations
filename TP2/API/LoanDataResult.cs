namespace WebApplication1;

public class LoanDataResult
{
    public int MonthlyRate { get; set; }
    public int MonthlyInsurance { get; set; }
    public int TotalInterest { get; set; }
    public int TotalInisurance { get; set; }
    public int RepaidAfter { get; set; }
}