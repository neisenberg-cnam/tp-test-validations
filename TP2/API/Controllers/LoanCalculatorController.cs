using Bank;
using Bank.Entities;
using Bank.Enums;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers;

[ApiController]
[Route("[controller]")]
public class LoanCalculatorController : ControllerBase
{

    private readonly ILogger<LoanCalculatorController> _logger;

    public LoanCalculatorController(ILogger<LoanCalculatorController> logger)
    {
        _logger = logger;
    }

    [HttpPost(Name = "GetLoanResult")]
    public IActionResult GetLoanResult(AskForLoanData data)
    {
        Loanee loanee = new Loanee(
        new Dictionary<InsuranceCriteria, bool>()
        {
            {InsuranceCriteria.Fit, data.Fit},
            {InsuranceCriteria.Smoker, data.Smoker},
            {InsuranceCriteria.HeartDisease, data.HeartDisease},
            {InsuranceCriteria.SoftwareEngineer, data.SoftwareEngineer},
            {InsuranceCriteria.FighterPilot, data.FighterPilot},
        }, 28);
        RateEnum rateType = RateEnum.Good;
        if (data.RateType == "VeryGood")
            rateType = RateEnum.VeryGood;
        else if (data.RateType == "Excellent")
            rateType = RateEnum.Excellent;
        LoanCalculator LoanCalculator = new LoanCalculator(data.Capital, loanee, data.Length, rateType);

        LoanDataResult results = new LoanDataResult();
        
        results.MonthlyRate = LoanCalculator.ReadableMonthlyRate();
        results.MonthlyInsurance = LoanCalculator.ReadableMonthlyInsuranceCost();
        results.TotalInisurance = LoanCalculator.ReadableRealInsuranceCost();
        results.TotalInterest = LoanCalculator.ReadableRealInterestCost();
        results.RepaidAfter = LoanCalculator.AmountRepaidAfterYears(data.NbYears);

        return Ok(results);
    }
}