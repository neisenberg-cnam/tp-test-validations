namespace Bank.Enums
{
    public enum InsuranceCriteria
    {
            Fit,
            Smoker,
            HeartDisease,
            SoftwareEngineer,
            FighterPilot,
    }
}