using System;
using System.Runtime.Serialization;

namespace Bank.Exceptions
{
    public class LoanAmountException : Exception
    {
        public LoanAmountException() {}

        public LoanAmountException(string? message) : base(message) {}
        
        public LoanAmountException(string? message, Exception? innerException) : base(message, innerException) {}

        protected LoanAmountException(SerializationInfo info, StreamingContext context) : base(info, context) {}

    }
}