using System;
using System.Runtime.Serialization;

namespace Bank.Exceptions
{
    public class LoanLengthException : Exception
    {
        public LoanLengthException() {}

        public LoanLengthException(string? message) : base(message) {}
        
        public LoanLengthException(string? message, Exception? innerException) : base(message, innerException) {}

        protected LoanLengthException(SerializationInfo info, StreamingContext context) : base(info, context) {}

    }
}