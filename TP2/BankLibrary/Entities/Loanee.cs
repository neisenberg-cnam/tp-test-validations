using System.Collections.Generic;
using Bank.Enums;

namespace Bank.Entities
{
    public class Loanee
    {
        public Dictionary<InsuranceCriteria, bool> Characteristics;
        public int Age { get; set; }

        public Loanee(Dictionary<InsuranceCriteria, bool> characteristics, int age)
        {
            Characteristics = characteristics;
            Age = age;
        }
    }
}