using Bank.Enums;
using Bank.Strategies;

namespace Bank.Entities
{
    public class Rate
    {
        private ConcreteRateStrategy RateStrategy;
        public int LoanLength { get; set; }

        public Rate()
        {
            LoanLength = 0;
        }

        public double GetRate()
        {
            return RateStrategy.GiveAppropriateRate(LoanLength);
        }

        public void SetRateStrategy(RateEnum rate)
        {
            if (rate == RateEnum.Good)
            {
                this.RateStrategy = new GoodRateStrategy();
            }
            if (rate == RateEnum.VeryGood)
            {
                this.RateStrategy = new VeryGoodRateStrategy();
            }
            if (rate == RateEnum.Excellent)
            {
                this.RateStrategy = new ExcellentRateStrategy();
            }
        }
    }
}