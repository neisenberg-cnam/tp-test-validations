using Bank.Entities;
using Bank.Enums;

namespace Bank
{
    public class InsuranceCalculator
    {
        private static Dictionary<InsuranceCriteria, double> RateByCriteria =
            new Dictionary<InsuranceCriteria, double>()
            {
                {InsuranceCriteria.Fit, -0.05d},
                {InsuranceCriteria.Smoker, 0.15d},
                {InsuranceCriteria.HeartDisease, 0.3d},
                {InsuranceCriteria.SoftwareEngineer, -0.05d},
                {InsuranceCriteria.FighterPilot, 0.15d},
            };
        
        public Loanee Loanee { get; set; }

        public InsuranceCalculator(Loanee loanee)
        {
            Loanee = loanee;
        }

        public double GetInsuranceRate()
        {
            double rate = 0.3d;
            foreach (var charac in Loanee.Characteristics)
            {
                if (RateByCriteria.ContainsKey(charac.Key) && charac.Value)
                {
                     rate += RateByCriteria[charac.Key];
                }
            }

            return  rate;
        }
    }
}