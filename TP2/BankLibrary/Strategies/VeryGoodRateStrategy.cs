using System.Collections.Generic;

namespace Bank.Strategies
{
    public class VeryGoodRateStrategy : ConcreteRateStrategy
    {
        public VeryGoodRateStrategy() : base()
        {
            ActualRates = new List<double> {0.43f, 0.55f, 0.73f, 0.91f, 1.15f};
        }
    }
}