using System.Collections.Generic;

namespace Bank.Strategies
{
    public class GoodRateStrategy : ConcreteRateStrategy
    {
        public GoodRateStrategy() : base()
        {
            ActualRates = new List<double> {0.62d, 0.67d, 0.85d, 1.04d, 1.27d};
        }
    }
}