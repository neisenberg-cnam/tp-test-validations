using System.Collections.Generic;

namespace Bank.Strategies
{
    public class ConcreteRateStrategy : IRateStrategy
    {
        protected static readonly List<int> LoanLengthThreshold = new List<int>() {7, 10, 15, 20, 25};
        protected List<double> ActualRates;
        
        public double GiveAppropriateRate(int len)
        {
            return ActualRates[GetIndexOfThresholdWithLength(len)];
        }

        private int GetIndexOfThresholdWithLength(int loanLength)
        {
            int index = -1;
            foreach (int len in LoanLengthThreshold)
            {
                if (loanLength >= len)
                {
                    index++;
                }
            }

            return (index >= 0) ? index : 0; 
        }
    }
}