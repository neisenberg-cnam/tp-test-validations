using System.Collections.Generic;

namespace Bank.Strategies
{
    public class ExcellentRateStrategy : ConcreteRateStrategy
    {
        public ExcellentRateStrategy() : base()
        {
            ActualRates = new List<double> {0.35d, 0.45d, 0.58d, 0.73d, 0.89d};
        }
    }
}