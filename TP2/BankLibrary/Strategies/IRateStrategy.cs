namespace Bank.Strategies
{
    public interface IRateStrategy
    {
        double GiveAppropriateRate(int len);
    }
}