using System;
using Bank.Entities;
using Bank.Enums;
using Bank.Exceptions;

namespace Bank
{
    public class LoanCalculator
    {
        private double Capital { get; set; }
        private int Length { get; set; }
        private Rate Rate { get; }
        private Loanee Loanee { get; set; }
        private InsuranceCalculator Insurance { get; set; }

        public LoanCalculator(double capital, Loanee loanee, int len, RateEnum rateEnum)
        {
            Rate = new Rate();
            Rate.SetRateStrategy(rateEnum);
            Loanee = loanee;
            SetCapitalAmount(capital);
            SetLoanLength(len);
            Insurance = new InsuranceCalculator(Loanee);
        }

        public void SetCapitalAmount(double capital)
        {
            if (capital < 50000f)
            {
                throw new LoanAmountException("Minimum capital amount is 50.000");
            }

            Capital = capital;
        }
        public void SetLoanLength(int len)
        {
            if (len < 9 || len > 25)
            {
                throw new LoanLengthException("Loan length should be between 9 and 25");
            }
            Length = len;
            Rate.LoanLength = len;
        }

        public double GetMonthlyInterestPayment()
        {
            double monthlyRate = (Rate.GetRate() / 100d) / 12d;
            double numerator = Capital * monthlyRate;
            double denominator = 1.0d - Math.Pow(1.0d + monthlyRate, -LengthInMonths());

            return numerator / denominator;
        }

        public int ReadableMonthlyInterestPayment()
        {
            return (int) Math.Round(GetMonthlyInterestPayment());
        }


        public double GetMonthlyInsuranceCost()
        {
            return Capital * (Insurance.GetInsuranceRate() / 100d) / 12d;
        }

        public int ReadableMonthlyInsuranceCost()
        {
            return (int) Math.Round(GetMonthlyInsuranceCost());
        }
        
        public int ReadableMonthlyRate()
        {
            return (int) Math.Round(GetMonthlyInsuranceCost() + GetMonthlyInterestPayment());
        }

        public int ReadableRealInterestCost()
        {
            double interestMonthly = GetMonthlyInsuranceCost() + GetMonthlyInterestPayment();
            double totalInterest = interestMonthly * Length * 12d;
            return (int) Math.Round(totalInterest - Capital);
        }

        public int ReadableRealInsuranceCost()
        {
            return (int) Math.Round(GetMonthlyInsuranceCost() * LengthInMonths());
        }

        public int AmountRepaidAfterYears(int years)
        {
            if (years < 1)
                return 0;
            return ReadableMonthlyRate() * 12 * years;
        }

        private int LengthInMonths()
        {
            return Length * 12;
        }
    }
}